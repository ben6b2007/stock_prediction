# Stock Prdeiction
## Objective
#### Find out the trend in Stock/FX/Crypto markets based on date and time
### Program Concept
#### The program is inspired by Youtuber,QuantRaiser (https://www.youtube.com/@quantraiser5478)
#### The program concept is assumed that the price stock must be growing up or falling down in some period of every year.
#### For example, according to the result, from 3rd low to 31st of January, it must be growing up 0.5% and the max increase range is 11%
<img src='demo.png'/>

## Strategy
#### According to the result, we can knows which period will be increase & down. Therefore, we can using the credit spread option strategy to take the profit.
#### For example, if spy low price is $400 in 2023-01-03, we can assumed that spy the increase range should not excceeded $444.
#### We can short call $444 in spy & long call $444.5 in spy. All the expired date is 2023-01-31. Assumed that $444 call spy price is $1, $444.5 call spy price is 0.5, Each credit spread, can be maked $0.5 profit. 
## Program structure
#### This program is performed by Python 3.8.5 (Jupyter notebook)
1. Store the train data in Data folder (Data can be download from yahoo finance)
2. opt_str.ipynb <---- Run All scripts from top to bottom, before run the 'run' script, change the location of Data folder & train data file name
3. Train the data, it will take a lot of time.
4. validator.ipynb <---- Run the script can be verified the trained data is work or not.