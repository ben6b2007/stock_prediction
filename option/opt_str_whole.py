import pandas as pd
import glob
import os
from statistics import mean
import numpy as np
from tqdm import trange, tqdm

def whole_abs_change(df1,c1,c2,start,end,end_day):
    price_method = []
    d1_period = []
    d2_period = []
    start_time = []
    end_time = []
    min_abs_change = []
    sec_abs_change = []
    third_abs_change = []
    abs_times = []
    mo1_period = []
    mo2_period = []
    print(c1,"+",c2,":")
    #with tqdm(total=12*12*31*31*(end-start)) as pbar:
    for mo1 in trange(1,13,position = 0,desc=f'{c1}-{c2}-Total Process'):
        for mo2 in np.arange(mo1,13):
            for d1 in trange(1,end_day,desc=f'start_month--{mo1}--end_month--{mo2}'):
                for d2 in np.arange(1,end_day):
                    abs_change = []
                    for st in np.arange(start,end+1):
                        con = (df1['Date'].dt.year==st)&(df1['Date'].dt.month>=mo1)&(df1['Date'].dt.month<=mo2)&(df1['Date'].dt.day>=d1)&(df1['Date'].dt.day<=d2)
                        temp_df = df1.loc[con]
                        if(len(temp_df)!=0):
                            temp_df.index = range(len(temp_df))
                            old = temp_df.iloc[0][c1]
                            up_new = max(temp_df[c2])
                            down_new = min(temp_df[c2])
                            if(abs(up_new)>=abs(down_new)):
                                temp = (up_new-old)/old*100
                                abs_change.append(abs(np.round(temp,5)))
                            else:
                                temp = (down_new-old)/old*100
                                abs_change.append(abs(np.round(temp,5)))
                            #pbar.update(1)
                    d1_period.append(d1)
                    d2_period.append(d2)
                    mo1_period.append(mo1)
                    mo2_period.append(mo2)
                    abs_times.append(len(abs_change))
                    if(len(abs_change)!=0):
                        min_abs_change.append(min(abs_change))
                    else:
                        min_abs_change.append(None)
                    if(len(abs_change)>=2):
                        sec_abs_change.append(sorted(abs_change)[1])
                    else:
                        sec_abs_change.append(None)
                    if(len(abs_change)>=3):
                        third_abs_change.append(sorted(abs_change)[2])
                    else:
                        third_abs_change.append(None)
                    start_time.append(c1)
                    end_time.append(c2)
            result = {'start_time':start_time,
                      'end_time':end_time,
                      'D1': d1_period,
                      'D2': d2_period, 
                      'mo1':mo1_period,
                      'mo2':mo2_period,
                      'abs_times': abs_times, 
                      'min_abs_change':min_abs_change,
                      'sec_abs_change':sec_abs_change,
                      'third_abs_change':third_abs_change
                     }
    result_df = pd.DataFrame(result)
    print("Done.")
    return result_df 
	
def whole_abs_run(filename,df1):
    path = f'C:/Users/ben/Desktop/stock/option/{filename}'
    if not os.path.exists(path):
        os.makedirs(path)
    os.chdir(path)
    print(df)
    path_month = f'{path}/{filename}'
    if not os.path.exists(path_month):
        os.makedirs(path_month)

    os.chdir(path_month)
    print(os.getcwd())
    start = df1['Date'].dt.year[0]
    end = df1.iloc[-1]['Date'].year
    end_day = 32
    oo_result = whole_abs_change(df1,'Open','Open',start,end,end_day)

    oh_result = whole_abs_change(df1,'Open','High',start,end,end_day)

    ol_result = whole_abs_change(df1,'Open','Low',start,end,end_day)

    oc_result = whole_abs_change(df1,'Open','Close',start,end,end_day)

    hl_result = whole_abs_change(df1,'High','Low',start,end,end_day)

    hc_result = whole_abs_change(df1,'High','Close',start,end,end_day)

    ho_result = whole_abs_change(df1,'High','Open',start,end,end_day)

    hh_result = whole_abs_change(df1,'High','High',start,end,end_day)

    cc_result = whole_abs_change(df1,'Close','Close',start,end,end_day)

    ch_result = whole_abs_change(df1,'Close','High',start,end,end_day)

    cl_result = whole_abs_change(df1,'Close','Low',start,end,end_day)

    co_result = whole_abs_change(df1,'Close','Open',start,end,end_day)

    lc_result = whole_abs_change(df1,'Low','Close',start,end,end_day)

    lh_result = whole_abs_change(df1,'Low','High',start,end,end_day)

    lo_result = whole_abs_change(df1,'Low','Open',start,end,end_day)

    ll_result = whole_abs_change(df1,'Low','Low',start,end,end_day)

    result_df = pd.concat([oo_result,oh_result,ol_result,oc_result,hl_result,hc_result,ho_result,hh_result,cc_result,ch_result,cl_result,co_result,lh_result,lc_result,lo_result,ll_result])
        
    result_df.to_csv(f'whole_abs_{filename}_abs.csv',index=False)
	
# main
path = 'C:/Users/ben/Desktop/stock/option/Data'
all_files = glob.glob(path + "/*.csv")
print(all_files)
for file in all_files:
    df = pd.read_csv(file,index_col=False)
    df.dropna()
    df['Date'] = pd.to_datetime(df['Date'], format='%Y-%m-%d')
    df = df.dropna()
    filename = os.path.basename(file).split(".csv")[0]
    #run(filename,df)
    whole_abs_run(filename,df)